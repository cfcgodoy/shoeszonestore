Rails.application.routes.draw do
  resources :products
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/makeLogin', to: "login#makeLogin"
  get '/productDetails/:id', to: "products#show"
end