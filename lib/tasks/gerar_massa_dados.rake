namespace :gerar_massa_dados do
    desc "Realizar requisições"
    task gerar: :environment do
        require 'net/http'
        require 'uri'
        require 'json'
        require 'faker'
        
        # LOGIN
        ([*0..3].sample * Time.now.hour).times do
            uri = URI.parse("http://localhost:3000/makeLogin")
            header = {'Content-Type': 'application/json'}
            user = {
                user: Faker::Internet.email,
                password: '12345'
            }
            http = Net::HTTP.new(uri.host, uri.port)
            request = Net::HTTP::Post.new(uri.request_uri, header)
            request.body = user.to_json
            
            # Send the request
            response = http.request(request)
        end

        # CONSULTA PRODUTO
        ([*0..3].sample * Time.now.hour).times do
            idProduto = Product.all.sample.id
            uri = URI.parse("http://localhost:3000/productDetails/#{idProduto}")
            header = {'Content-Type': 'application/json'}
            http = Net::HTTP.new(uri.host, uri.port)
            request = Net::HTTP::Get.new(uri.request_uri, header)
            
            # Send the request
            response = http.request(request)
        end

        # CUSTOM EVENT COMPRA
        ([*0..3].sample * Time.now.hour).times do
            idProduto = Product.all.sample.id
            ::NewRelic::Agent.record_custom_event(
                'Compras',
                parceiroDePagamento: ['PayPal', 'PayPal', 'MercadoPago', 'MercadoPago', 'MercadoPago', 'GetNet'].sample,
                quantidadeDeItensSobrePedido: [*1..15].sample,
                valorTotalPedido: [*1..1000].sample*0.33,
                cidade: ['Piracicaba', 'Americana', 'Limeira', 'Campinas'].sample,
                estado: 'SP'
            )
            ::NewRelic::Agent.record_custom_event(
                'Compras',
                parceiroDePagamento: ['PayPal', 'PayPal', 'MercadoPago', 'MercadoPago', 'MercadoPago', 'GetNet'].sample,
                quantidadeDeItensSobrePedido: [*1..15].sample,
                valorTotalPedido: [*1..1000].sample*0.33,
                cidade: ['Extrema', 'Belo Horizonte', 'Betim', 'Contagem', 'Ouro Preto'].sample,
                estado: 'MG'
            )   
        end
        ::NewRelic::Agent.record_custom_event(
            'Compras',
            parceiroDePagamento: ['PayPal', 'PayPal', 'MercadoPago', 'MercadoPago', 'MercadoPago', 'GetNet'].sample,
            quantidadeDeItensSobrePedido: [*1..15].sample,
            valorTotalPedido: [*1..1000].sample*0.33,
            cidade: ['Curitiba'],
            estado: 'RS'
        )
        

    end
end
