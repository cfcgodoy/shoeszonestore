class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.float :value
      t.string :category
      t.integer :totalUnits

      t.timestamps
    end
  end
end
